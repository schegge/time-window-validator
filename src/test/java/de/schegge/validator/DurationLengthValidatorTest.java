package de.schegge.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Set;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@ExtendWith(ValidatorParameterResolver.class)
class DurationLengthValidatorTest {

  private static class DurationDto {

    @DurationLength(minDuration = "P2D", maxDuration = "P4D")
    Duration minMaxDuration;

    @DurationLength(minDuration = "P2D")
    Duration minDuration;

    @DurationLength(maxDuration = "P4D")
    Duration maxDuration;

    @DurationLength(min = 2L, max = 4L, unit = ChronoUnit.DAYS)
    Duration minMax;

    @DurationLength(min = 2L, unit = ChronoUnit.DAYS)
    Duration min;

    @DurationLength(max = 4L, unit = ChronoUnit.DAYS)
    Duration max;

    @DurationLength(min = 172800L)
    Duration minSeconds;

    @DurationLength(max = 345600L)
    Duration maxSeconds;
  }

  @ParameterizedTest
  @CsvSource({"P2D", "P3D", "P4D"})
  void validDuration(Duration duration, Validator validator) {
    DurationDto dto = new DurationDto();
    dto.minMaxDuration = duration;
    dto.minDuration = duration;
    dto.maxDuration = duration;
    dto.min = duration;
    dto.max = duration;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @ParameterizedTest
  @CsvSource({"P1D", "P5D"})
  void invalidDuration(Duration duration, Validator validator) {
    DurationDto dto = new DurationDto();
    dto.minMaxDuration = duration;
    dto.minDuration = duration;
    dto.maxDuration = duration;
    dto.minMax = duration;
    dto.min = duration;
    dto.max = duration;
    dto.maxSeconds = duration;
    dto.minSeconds = duration;
    Set<ConstraintViolation<DurationDto>> validate = validator.validate(dto);
    assertFalse(validate.isEmpty());
    assertEquals(5, validate.size());
  }

  @Test
  void testNull(Validator validator) {
    assertTrue(validator.validate(new DurationDto()).isEmpty());
  }
}
