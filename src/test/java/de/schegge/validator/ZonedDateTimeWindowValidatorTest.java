package de.schegge.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZonedDateTime;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ValidatorParameterResolver.class)
class ZonedDateTimeWindowValidatorTest {

  private static class ZonedDateTimeDto {

    @FutureWindow("P2D")
    ZonedDateTime future;

    @FuturePresentWindow("P2D")
    ZonedDateTime futurePresent;

    @PastWindow("P2D")
    ZonedDateTime past;

    @PastPresentWindow("P2D")
    ZonedDateTime pastPresent;
  }

  @Test
  void testCorrectOffsetDateTimeFuture(Validator validator) {
    ZonedDateTime future = ZonedDateTime.now().plusDays(1L);
    System.out.println();
    ZonedDateTimeDto dto = new ZonedDateTimeDto();
    dto.future = future;
    dto.futurePresent = future;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectOffsetDateTimePresent(Validator validator) {
    ZonedDateTime now = ZonedDateTime.now();
    ZonedDateTimeDto dto = new ZonedDateTimeDto();
    dto.pastPresent = now;
    dto.futurePresent = now;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectLocalDatePast(Validator validator) {
    ZonedDateTime past = ZonedDateTime.now().minusDays(1L);
    ZonedDateTimeDto dto = new ZonedDateTimeDto();
    dto.past = past;
    dto.pastPresent = past;
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testIncorrectOffsetDateTimeFuture(Validator validator) {
    ZonedDateTime past = ZonedDateTime.now().minusDays(1L);
    ZonedDateTimeDto dto = new ZonedDateTimeDto();
    dto.future = past;
    dto.futurePresent = past;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testIncorrectOffsetDateTimePresent(Validator validator) {
    ZonedDateTime now = ZonedDateTime.now();
    ZonedDateTimeDto dto = new ZonedDateTimeDto();
    dto.past = now;
    dto.future = now;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testIncorrectLocalDatePast(Validator validator) {
    ZonedDateTime now = ZonedDateTime.now().plusDays(1L);
    ZonedDateTimeDto dto = new ZonedDateTimeDto();
    dto.past = now;
    dto.pastPresent = now;
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(2, validator.validate(dto).size());
  }

  @Test
  void testNull(Validator validator) {
    assertTrue(validator.validate(new ZonedDateTimeDto()).isEmpty());
  }
}
