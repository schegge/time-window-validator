package de.schegge.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.temporal.ChronoUnit;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR,
    ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {})
public @interface DurationLength {

  String message() default "{de.schegge.validator.DurationLength.message}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

  String value() default "";

  String minDuration() default "";

  String maxDuration() default "";

  long min() default 0L;

  long max() default 0L;

  ChronoUnit unit() default ChronoUnit.SECONDS;
}